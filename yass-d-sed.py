#!/usr/bin/env python3
#
# ==========================================================================
# yass-d-sed.py -- version 0.0.99.20231020
#
# written by cleemy desu wayo / Licensed under CC0 1.0
#
# official repository: https://gitlab.com/cleemy-desu-wayo/yass
# ==========================================================================

import sys
import re

def error_exit(error_msg):
    print(f'ERROR: {error_msg}', file=sys.stderr)
    sys.exit(1)

if len(sys.argv) <= 1:
    error_exit('lack of sed code')

if not sys.argv[1].isascii():
    error_exit('invalid sed code (contains non-ASCII characters)')

if len(sys.argv[1]) % 2 == 1:
    error_exit('sed code must have an even number of bytes')

sed_code_list = []
token_list = ['s']
token = ''
mode = ':'

tmp_list = re.split('(..)', sys.argv[1])[1::2]

for i, char_pair in enumerate(tmp_list):
    identifier = char_pair[0]
    char       = char_pair[1]

    if i == 0:
        if identifier != ':':
            error_exit('invalid sed code (1)')

    if mode == '_':
        if identifier == ':':
            token_list = ['s']
            token = ''
            mode = ':'
        else:
            error_exit('invalid sed code (2)')

    if mode == ':':
        if identifier == ':':
            token = token + char
        elif identifier == '.':
            token_list.append(token)
            token = ''
            mode = '.'
        elif identifier == '_':
            token_list.append(token)
            token_list.append('')
            mode = '_'
        else:
            error_exit('invalid sed code (3)')

    if mode == '.':
        if identifier == '.':
            token = token + char
        elif identifier == '_':
            token_list.append(token)
            mode = '_'
        else:
            error_exit('invalid sed code (4)')

    if identifier == '_':
        if char == 'g':
            token_list.append('g')
        elif char == '_':
            token_list.append('')
        else:
            error_exit('invalid sed code (5)')
        sed_code_list.append(token_list)

if mode != '_':
    error_exit('invalid sed code (6)')

for line in [s.rstrip('\r\n') for s in sys.stdin]:
    replaced_line = line
    for token_list in sed_code_list:
        if token_list[3] == 'g':
            replaced_line = replaced_line.replace(token_list[1], token_list[2])
        else:
            replaced_line = replaced_line.replace(token_list[1], token_list[2], 1)
    print(replaced_line)

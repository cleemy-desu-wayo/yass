#!/usr/bin/env python3
#
# ==========================================================================
# yass-b-sed.py -- version 0.0.99.20231019
#
# written by cleemy desu wayo / Licensed under CC0 1.0
#
# official repository: https://gitlab.com/cleemy-desu-wayo/yass
# ==========================================================================

import sys

if len(sys.argv) <= 2 or len(sys.argv) % 2 == 0:
    print("ERROR: lack of sed code", file=sys.stderr)
    sys.exit(1)

argv_list = sys.argv
argv_list.pop(0)
sed_code_list = []

for i, argv_str in enumerate(argv_list):
    if i % 2 == 0:
        if argv_str != '-e':
            print("ERROR: invalid option", file=sys.stderr)
            sys.exit(1)
    else:
        token_list = argv_str.split('/')
        if len(token_list) == 4 and token_list[0] == 's' and (token_list[3] == '' or token_list[3] == 'g'):
            sed_code_list.append(token_list)
        else:
            print("ERROR: invalid sed code", file=sys.stderr)
            sys.exit(1)

for line in [s.rstrip('\r\n') for s in sys.stdin]:
    replaced_line = line
    for token_list in sed_code_list:
        if token_list[3] == 'g':
            replaced_line = replaced_line.replace(token_list[1], token_list[2])
        else:
            replaced_line = replaced_line.replace(token_list[1], token_list[2], 1)
    print(replaced_line)

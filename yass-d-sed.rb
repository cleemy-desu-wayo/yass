#!/usr/bin/env ruby
#
# ==========================================================================
# yass-d-sed.rb -- version 0.0.99.20231020
#
# written by cleemy desu wayo / Licensed under CC0 1.0
#
# official repository: https://gitlab.com/cleemy-desu-wayo/yass
# ==========================================================================

def error_exit(error_msg)
  raise "ERROR: #{error_msg}"
end

if ARGV.length < 1
   error_exit('lack of sed code')
end
if not ARGV[0].ascii_only?
  error_exit('invalid sed code (contains non-ASCII characters)')
end
if ARGV[0].length % 2 == 1
  error_exit('sed code must have an even number of bytes')
end

sed_code_list = []
token_list = ['s']
token = ''
mode = ':'

tmp_list = ARGV[0].each_char.each_slice(2)

tmp_list.each_with_index do |char_pair, i|
  identifier = char_pair[0]
  char       = char_pair[1]

  if i == 0
    if identifier != ':'
      error_exit('invalid sed code (1)')
    end
  end

  if mode == '_'
    if identifier == ':'
      token_list = ['s']
      token = ''
      mode = ':'
    else
      error_exit('invalid sed code (2)')
    end
  end

  if mode == ':'
    if identifier == ':'
      token << char
    elsif identifier == '.'
      token_list << token
      token = ''
      mode = '.'
    elsif identifier == '_'
      token_list << token
      token_list << ''
      mode = '_'
    else
      error_exit('invalid sed code (3)')
    end
  end

  if mode == '.'
    if identifier == '.'
      token << char
    elsif identifier == '_'
      token_list << token
      mode = '_'
    else
      error_exit('invalid sed code (4)')
    end
  end

  if identifier == '_'
    if char == 'g'
      token_list << 'g'
    elsif char == '_'
      token_list << ''
    else
      error_exit('invalid sed code (5)')
    end
    sed_code_list << token_list
  end
end

if mode != '_'
  error_exit('invalid sed code (6)')
end

STDIN.readlines.each do |line|
  replaced_line = line
  sed_code_list.each do |token_list|
    if token_list[3] == 'g'
      replaced_line = replaced_line.gsub(token_list[1], token_list[2])
    else
      replaced_line = replaced_line.sub(token_list[1], token_list[2])
    end
  end
  puts replaced_line
end

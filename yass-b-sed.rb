#!/usr/bin/env ruby
#
# ==========================================================================
# yass-b-sed.rb -- version 0.0.99.20231019.1
#
# written by cleemy desu wayo / Licensed under CC0 1.0
#
# official repository: https://gitlab.com/cleemy-desu-wayo/yass
# ==========================================================================

if ARGV.length < 2 || ARGV.length % 2 == 1
  raise 'ERROR: lack of sed code'
end

sed_code_list = []

ARGV.each_with_index do |argv_str, i|
  if i % 2 == 0
    raise 'ERROR: invalid option' unless argv_str == '-e'
  else
    token_list = argv_str.split('/', -1)
    if token_list.length == 4 && token_list[0] == 's' && (token_list[3] == '' || token_list[3] == 'g')
      sed_code_list << token_list
    else
      raise 'ERROR: invalid sed code'
    end
  end
end

STDIN.readlines.each do |line|
  replaced_line = line
  sed_code_list.each do |token_list|
    if token_list[3] == 'g'
      replaced_line = replaced_line.gsub(token_list[1], token_list[2])
    else
      replaced_line = replaced_line.sub(token_list[1], token_list[2])
    end
  end
  puts replaced_line
end

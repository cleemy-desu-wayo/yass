#!/usr/bin/env python3
#
# ==========================================================================
# yass-a-sed.py -- version 0.0.99.20231019.2
#
# written by cleemy desu wayo / Licensed under CC0 1.0
#
# official repository: https://gitlab.com/cleemy-desu-wayo/yass
# ==========================================================================

import sys

if len(sys.argv) <= 1:
    print("ERROR: lack of sed code", file=sys.stderr)
    sys.exit(1)

sed_code_list = []

tmp_list = [s.strip() for s in sys.argv[1].split(';')]
tmp_list = [s for s in tmp_list if s != '']

for sed_code in tmp_list:
    token_list = sed_code.split('/')
    if len(token_list) == 4 and token_list[0] == 's' and (token_list[3] == '' or token_list[3] == 'g'):
        sed_code_list.append(token_list)
    else:
        print("ERROR: invalid sed code", file=sys.stderr)
        sys.exit(1)

for line in [s.rstrip('\r\n') for s in sys.stdin]:
    replaced_line = line
    for token_list in sed_code_list:
        if token_list[3] == 'g':
            replaced_line = replaced_line.replace(token_list[1], token_list[2])
        else:
            replaced_line = replaced_line.replace(token_list[1], token_list[2], 1)
    print(replaced_line)

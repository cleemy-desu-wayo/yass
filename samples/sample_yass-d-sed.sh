#!/bin/sh
#
# ==========================================================================
# sample/sample_yass-d-sed.sh (2024-01-26)
#
# written by cleemy desu wayo / Licensed under CC0 1.0
#
# official repository: https://gitlab.com/cleemy-desu-wayo/yass
# ==========================================================================
#
# * basic usage of yass-d-sed:
#
#   $ echo hoooooge | ./yass-d-sed.py ':o.O__'
#   hOooooge
#   $ echo hoooooge | ./yass-d-sed.py ':o.O_g'
#   hOOOOOge
#   $ echo hoooooge | ./yass-d-sed.py ':o:o:o.O.O.O__'
#   hOOOooge
#   $ echo hoooooge | ./yass-d-sed.py ':o:o:o.1.2.3__'
#   h123ooge
#   $ echo hoooooge | ./yass-d-sed.py ':o:g:e.O.G.E__'
#   hooooOGE
#   $ echo hoooooge | ./yass-d-sed.py ':o:g:e.O.G.E__:o.x_g'
#   hxxxxOGE
#

LANG=C   ; export LANG
LC_ALL=C ; export LC_ALL

user="$1"
pass="$2"

before_str=$( printf '%s:%s:\n' "${user}" "${pass}"        | sed 's/./:&/g')
after_str=$(  printf 'password of %s is valid\n' "${user}" | sed 's/./\.&/g')

#echo "before: ${before_str}"    # DEBUG
#echo "after:  ${after_str}"     # DEBUG

sed='./yass-d-sed.py'   # Python edition of yass-d-sed
#sed='./yass-d-sed.rb'   # Ruby edition of yass-d-sed

cat samples/passwd.txt \
      | grep -v ' ' \
      | grep "^${user}:" \
      | ${sed} "${before_str}${after_str}__" \
      | grep ' is valid'

exit $?

#!/bin/sh
#
# ==========================================================================
# sample/sample_yass-a-sed.sh (2024-01-23)
#
# written by cleemy desu wayo / Licensed under CC0 1.0
#
# official repository: https://gitlab.com/cleemy-desu-wayo/yass
# ==========================================================================
#
# * basic usage of yass-a-sed:
#
#   $ echo hoooooge | ./yass-a-sed.py 's/o/O/'
#   hOooooge
#   $ echo hoooooge | ./yass-a-sed.py 's/o/O/g'
#   hOOOOOge
#   $ echo hoooooge | ./yass-a-sed.py 's/o/O/g;s/e/E/'
#   hOOOOOgE
#

LANG=C   ; export LANG
LC_ALL=C ; export LC_ALL

user="$1"
pass="$2"

sed='./yass-a-sed.py'   # Python edition of yass-a-sed
#sed='./yass-a-sed.rb'   # Ruby edition of yass-a-sed
#sed='sed'               # system sed
#sed='busybox sed'       # busybox sed

cat samples/passwd.txt \
      | grep -v ' ' \
      | grep "^${user}:" \
      | ${sed} "s/${user}:${pass}:/password of ${user} is valid/" \
      | grep ' is valid'

exit $?

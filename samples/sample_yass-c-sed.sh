#!/bin/sh
#
# ==========================================================================
# sample/sample_yass-c-sed.sh (2024-01-25)
#
# written by cleemy desu wayo / Licensed under CC0 1.0
#
# official repository: https://gitlab.com/cleemy-desu-wayo/yass
# ==========================================================================
#
# * requirements:
#
#   * /dev/urandom
#   * modern od command
#
# * basic usage of yass-c-sed:
#
#   $ echo hoooooge | ./yass-c-sed.py '/x/;s/x/o/x/O/x/g'
#   hOOOOOge
#   $ echo hoooooge | ./yass-c-sed.py '/lZykvCHcaw/;s/lZykvCHcaw/o/lZykvCHcaw/O/lZykvCHcaw/g'
#   hOOOOOge
#

LANG=C   ; export LANG
LC_ALL=C ; export LC_ALL

user="$1"
pass="$2"

chars='abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ'
chars2=$(echo "${chars}${chars}${chars}${chars}${chars}" | fold -w 1 | awk '{ print NR ":" $1 }')

rand_list=$(dd if=/dev/urandom bs=1 count=10 2>/dev/null | od -v -tu1 -An)

separator='/'
for char in ${rand_list}
do
  new_char=$(printf "%s\n" "${chars2}" | grep "^${char}:" | head -1 | sed 's/.*://')
  separator="${separator}${new_char}"
done
separator="${separator}/"

#separator='/x/'  # DEBUG: intentionally weak

#printf '%s\n' "${separator}"  # DEBUG

sed='./yass-c-sed.py'   # Python edition of yass-c-sed
#sed='./yass-c-sed.rb'   # Ruby edition of yass-c-sed

cat samples/passwd.txt \
      | grep -v ' ' \
      | grep "^${user}:" \
      | ${sed} "${separator};s${separator}${user}:${pass}:${separator}password of ${user} is valid${separator}" \
      | grep ' is valid'

exit $?

#!/usr/bin/env ruby
#
# ==========================================================================
# samples/yass_webui.rb (2024-01-27)
#
# written by cleemy desu wayo / Licensed under CC0 1.0
#
# official repository: https://gitlab.com/cleemy-desu-wayo/yass
# ==========================================================================
#
# * how to start:
#
#   1. put main body of yass in the current directory (yass-a-sed.py, etc.)
#   2. put samples (including this script) to samples/ directory
#   3. run this script:
#      $ samples/yass_webui.rb 8080
#   4. access to the following on browser:
#      http://localhost:8080/yass
#

require 'webrick'
require 'open3'

webui_port = '8080'
webui_port = ARGV[0] if ARGV[0]
puts "webui_port: #{webui_port}"

repository_url         = 'https://gitlab.com/cleemy-desu-wayo/yass'
repository_url_escaped = WEBrick::HTMLUtils.escape(repository_url.to_s)
user_escaped = ''
pass_escaped = ''

websrv = WEBrick::HTTPServer.new({:DocumentRoot => './', :Port => webui_port})

websrv.mount_proc('/yass') do |req, res|
  r = req.query

  res.body = <<~EOS
        <!doctype html>
        <html><head>
        <meta charset="utf-8">
        <title>yass sample Web UI (2024-01-27)</title>
        <style>
          * {margin: 0 0 0 0.3rem;}
          h2 {margin: 1rem 0 0.2rem 0;}
          form {font-size: 120%; margin-top: 2rem;}
          input, button {font-size: 120%;}
          hr {margin: 1rem 0 1rem 0;}
          .result {max-width: 40rem; font-size: 120%; margin-left: 1.8rem; padding: 0.4rem; background-color: #cccccc;}
        </style></head><body>
        <h1>yass sample Web UI (2024-01-27)</h1>
        <p>written by cleemy desu wayo / Licensed under CC0 1.0</p>
        <p>official repository: <a href="#{repository_url_escaped}">#{repository_url_escaped}</a></p>
        <hr>
  EOS

  passwd_file_path    = 'samples/passwd.txt'
  res.body += "<h2>#{WEBrick::HTMLUtils.escape(passwd_file_path.to_s)}</h2>\n"

  if File.readable?(passwd_file_path)
    passwd_file_content = File.read(passwd_file_path)
    res.body += "<pre>#{WEBrick::HTMLUtils.escape(passwd_file_content.to_s)}</pre>\n"
  else
    res.body += "<p style=\"color:red\">ERROR: not found (or not readable)</p>\n"
  end

  res.body += "<hr>\n"

  if not r.empty?
    ("a".."d").each do |version|
      file_path = "samples/sample_yass-#{version}-sed.sh"
      res.body += "<h2>#{WEBrick::HTMLUtils.escape(file_path.to_s)}</h2>\n"

      # if not found (or not executable)
      if not File.executable?(file_path)
        res.body += "<p style=\"color:red\">ERROR: not found (or not executable)</p>\n"
        next
      end

      command_result, _, command_status = Open3.capture3(file_path, r['user'], r['pass'])
      res.body += <<~EOS
            <div class="result">
            <p>result: #{WEBrick::HTMLUtils.escape(command_result.to_s)}</p>
            <p>exit status: #{WEBrick::HTMLUtils.escape(command_status.exitstatus.to_s)}</p>
            </div>
      EOS
    end

    user_escaped = WEBrick::HTMLUtils.escape(r['user'].force_encoding("UTF-8"))
    pass_escaped = WEBrick::HTMLUtils.escape(r['pass'].force_encoding("UTF-8"))
  end

  res.body += <<~EOS
        <form method="get">
        <p>user:<input type="text" name="user" value="#{user_escaped}"></p>
        <p>pass:<input type="text" name="pass" value="#{pass_escaped}"></p>
        <button>send</button></form>
        </body></html>
  EOS
end

websrv.start

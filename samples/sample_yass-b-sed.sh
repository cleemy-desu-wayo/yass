#!/bin/sh
#
# ==========================================================================
# sample/sample_yass-b-sed.sh (2024-01-23)
#
# written by cleemy desu wayo / Licensed under CC0 1.0
#
# official repository: https://gitlab.com/cleemy-desu-wayo/yass
# ==========================================================================
#
# * basic usage of yass-b-sed:
#
#   $ echo hoooooge | ./yass-b-sed.py -e 's/o/O/'
#   hOooooge
#   $ echo hoooooge | ./yass-b-sed.py -e 's/o/O/g' -e 's/e/E/'
#   hOOOOOgE
#   $ echo hoooooge | ./yass-b-sed.py -e 's/o/O/g;s/e/E/'
#   ERROR: invalid sed code
#

LANG=C   ; export LANG
LC_ALL=C ; export LC_ALL

user="$1"
pass="$2"

sed='./yass-b-sed.py'   # Python edition of yass-b-sed
#sed='./yass-b-sed.rb'   # Ruby edition of yass-b-sed

cat samples/passwd.txt \
      | grep -v ' ' \
      | grep "^${user}:" \
      | ${sed} -e "s/${user}:${pass}:/password of ${user} is valid/" \
      | grep ' is valid'

exit $?

#!/usr/bin/env ruby
#
# ==========================================================================
# yass-c-sed.rb -- version 0.0.99.20231019
#
# written by cleemy desu wayo / Licensed under CC0 1.0
#
# official repository: https://gitlab.com/cleemy-desu-wayo/yass
# ==========================================================================

def error_exit(error_msg)
  raise "ERROR: #{error_msg}"
end

error_exit('lack of sed code') if ARGV.length < 1

if ARGV[0] !~ /\A(\/[0-9a-zA-Z]+\/);(.*)/
  error_exit('specify a delimiter at the beginning (such as "/a62z4E2hH2q/;")')
end

delimiter     = $1
sed_code_body = $2

sed_code_list = []
token_list = []

tmp_list = sed_code_body.split(delimiter, -1)

tmp_list.each_with_index do |token, i|
  if i == 0
    error_exit('invalid sed code (1)') unless token.lstrip == 's'
    token_list = ['s']
  elsif i % 3 == 0
    if token.start_with?('g')
      token_list << 'g'
    else
      token_list << ''
    end
    token = token.sub(/^g?\s*/, '')
    if i == (tmp_list.length - 1)
      error_exit('invalid sed code (2)') unless token =~ /^;?\s*$/
    else
      error_exit('invalid sed code (3)') unless token =~ /^;\s*s$/
    end
    sed_code_list << token_list
    token_list = ['s']
  elsif i % 3 == 1 or i % 3 == 2
    token_list << token
  end
end

STDIN.readlines.each do |line|
  replaced_line = line
  sed_code_list.each do |token_list|
    if token_list[3] == 'g'
      replaced_line = replaced_line.gsub(token_list[1], token_list[2])
    else
      replaced_line = replaced_line.sub(token_list[1], token_list[2])
    end
  end
  puts replaced_line
end

#!/usr/bin/env ruby
#
# ==========================================================================
# yass-a-sed.rb -- version 0.0.99.20231019.2
#
# written by cleemy desu wayo / Licensed under CC0 1.0
#
# official repository: https://gitlab.com/cleemy-desu-wayo/yass
# ==========================================================================

raise 'ERROR: lack of sed code' unless ARGV[0]

sed_code_list = []
tmp_list = ARGV[0].split(';').map(&:strip).reject(&:empty?)

tmp_list.each do |sed_code|
  token_list = sed_code.split('/', -1)
  if token_list.length == 4 && token_list[0] == 's' && (token_list[3] == '' || token_list[3] == 'g')
    is_valid_sed_code = true
    sed_code_list << token_list
  else
    raise 'ERROR: invalid sed code'
  end
end

STDIN.readlines.each do |line|
  replaced_line = line
  sed_code_list.each do |token_list|
    if token_list[3] == 'g'
      replaced_line = replaced_line.gsub(token_list[1], token_list[2])
    else
      replaced_line = replaced_line.sub(token_list[1], token_list[2])
    end
  end
  puts replaced_line
end
